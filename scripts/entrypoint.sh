#!/bin/sh

set -e #exit immediately on err

python manage.py collectstatic --noinput #noinput assumes yes to any questions
python manage.py wait_for_db
python manage.py migrate

uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi #master = run as master signal, threads = multi-threading